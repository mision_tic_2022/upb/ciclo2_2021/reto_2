import java.util.ArrayList;

public class Profesor {
    //Atributos
    private int id;
    private String nombre;
    private ArrayList<Curso> cursos;
    private int salario;

    //Constructor
    public Profesor(String nombre, int salario){
        this.nombre = nombre;
        this.salario = salario;
        this.cursos = new ArrayList<Curso>();
    }

    //Getters and Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Curso getCurso(int pos) {
        return cursos.get(pos);
    }

    public int getSizeCursos(){
        return cursos.size();
    }

    public void setCurso(int pos, Curso curso) {
        cursos.set(pos, curso);
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public void addCurso(Curso curso){
        this.cursos.add(curso);
    }


}
