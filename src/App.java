public class App {
    public static void main(String[] args) throws Exception {
        
        Profesor objProfesor = new Profesor("Raúl", 1875000);
        //Añadir Cursos
        objProfesor.addCurso( new Curso("Física") );
        objProfesor.addCurso( new Curso("Química"));
        objProfesor.addCurso( new Curso("Biología"));

        //Crear objeto colegio
        Colegio objColegio = new Colegio();
        //Añadir profesor al colegio
        objColegio.addProfesor(objProfesor);

        System.out.println( objColegio.liquidarNominaProfe(objProfesor));

    }
}
